#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *title_case(const char *s) {
    char buffer[BUFSIZ];

    strncpy(buffer, s, BUFSIZ);
    buffer[0] = toupper(buffer[0]);

    return buffer;
}

int main(int argc, char *argv[]) {
    for (int i = 1; i < argc; i++) {
    	puts(title_case(argv[i]));
    }

    return 0;
}
