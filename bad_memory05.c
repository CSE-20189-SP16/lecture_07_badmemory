#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *title_case(const char *s) {
    char *buffer = strdup(s);

    strncpy(buffer, s, strlen(s));
    buffer[0] = toupper(buffer[0]);

    return buffer;
}

int main(int argc, char *argv[]) {
    for (int i = 1; i < argc; i++) {
    	char *s = title_case(argv[i]);
    	puts(s);
    	free(s);
    }

    return 0;
}
