CC=		gcc
CFLAGS=		-g -gdwarf-2 -std=gnu99 -Wall
SOURCE=		$(wildcard *.c)
PROGRAMS=	$(SOURCE:.c=)

all:	$(PROGRAMS)

%:	%.c
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -f $(PROGRAMS)
